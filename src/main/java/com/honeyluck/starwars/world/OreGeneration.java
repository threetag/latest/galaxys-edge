package com.honeyluck.starwars.world;

import com.honeyluck.starwars.events.Registries;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.*;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.registries.ForgeRegistries;

public class OreGeneration {

    public static final Feature<ProbabilityConfig> KYBER_CRYSTALS = new KyberCrystalFeature(ProbabilityConfig::deserialize);

    public static void setupOreGeneration() {

        for(Biome biome : ForgeRegistries.BIOMES) {
            biome.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, biome.createDecoratedFeature(KYBER_CRYSTALS, new ProbabilityConfig(0.1F),
                    Placement.COUNT_RANGE, new CountRangeConfig(2, 0, 0, 20)));
            //Kyber crystals, 2 per chunk, y = 0 - 20
        }
    }
}
