package com.honeyluck.starwars;

import com.honeyluck.starwars.client.gui.ComponentConstructorScreen;
import com.honeyluck.starwars.client.gui.SaberConstructorScreen;
import com.honeyluck.starwars.client.render.RenderComponentConstructor;
import com.honeyluck.starwars.client.render.RenderSaberConstructor;
import com.honeyluck.starwars.client.render.entity.RenderEntityHolocron;
import com.honeyluck.starwars.client.render.layer.RenderLayerLightsaber;
import com.honeyluck.starwars.common.entity.EntityHolocron;
import com.honeyluck.starwars.common.tileentity.TileEntityComponentConstructor;
import com.honeyluck.starwars.common.tileentity.TileEntitySaberConstructor;
import com.honeyluck.starwars.events.EventHandler;
import com.honeyluck.starwars.network.MessageCraftComponent;
import com.honeyluck.starwars.world.OreGeneration;
import com.honeyluck.starwars.events.Registries;
import com.honeyluck.starwars.network.MessageUpdateSaberConstructor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.minecraftforge.items.CapabilityItemHandler;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Mod(StarWars.modid)
public class StarWars {
    public static final String modid = "galaxysedge";
    public static SimpleChannel NETWORK_CHANNEL;
    private static int networkId;
    public static ItemGroup tabItems = new ItemGroup("starwarsItems"){
        @Override
        public ItemStack createIcon() {
            return new ItemStack(Registries.sithHolocron);
        }
    };
    public static ItemGroup tabSaberParts = new ItemGroup("starwarsSaberParts") {
        @Override
        public ItemStack createIcon() {
            ItemStack stack = new ItemStack(Registries.skywalkerSaber);
            stack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(itemHandler -> {
                itemHandler.insertItem(0, new ItemStack(Registries.skywalkerSaberEmitter), false);
                itemHandler.insertItem(2, new ItemStack(Registries.skywalkerSaberPommel), false);
            });
            return stack;
        }
    };

    public StarWars() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);

        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
	        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);
        });
        MinecraftForge.EVENT_BUS.register(this);
        MinecraftForge.EVENT_BUS.register(new EventHandler());
    }
    private void setup(final FMLCommonSetupEvent event) {
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
            try {
                // Item Colors
                Minecraft.getInstance().getItemColors().register((IItemColor) Class.forName("com.honeyluck.starwars.common.item.ItemKyberCrystal$ItemColor").newInstance(), Registries.kyberCrystal);
                ScreenManager.registerFactory(Registries.saberConstructorContainer, SaberConstructorScreen.ModifyScreen::new);
	            ScreenManager.registerFactory(Registries.saberConstructorModeConstructContainer, SaberConstructorScreen.ConstructScreen::new);
	            ScreenManager.registerFactory(Registries.componentConstructorContainer, ComponentConstructorScreen::new);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        registerMessage(MessageUpdateSaberConstructor.class, MessageUpdateSaberConstructor::toBytes, MessageUpdateSaberConstructor::new, MessageUpdateSaberConstructor::handle);
        registerMessage(MessageCraftComponent.class, MessageCraftComponent::toBytes, MessageCraftComponent::new, MessageCraftComponent::handle);
        OreGeneration.setupOreGeneration();

    }

    @OnlyIn(Dist.CLIENT)
    private void doClientStuff(final FMLClientSetupEvent event) {
    	DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
		    ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySaberConstructor.class, new RenderSaberConstructor());
		    ClientRegistry.bindTileEntitySpecialRenderer(TileEntityComponentConstructor.class, new RenderComponentConstructor());

            RenderingRegistry.registerEntityRenderingHandler(EntityHolocron.class, RenderEntityHolocron::new);

            Map<String, PlayerRenderer> skinMap = Minecraft.getInstance().getRenderManager().getSkinMap();
            for (PlayerRenderer renderPlayer : skinMap.values()) {
                renderPlayer.addLayer(new RenderLayerLightsaber(renderPlayer));
            }
	    });
    }

    public static <MSG> int registerMessage(Class<MSG> messageType, BiConsumer<MSG, PacketBuffer> encoder, Function<PacketBuffer, MSG> decoder, BiConsumer<MSG, Supplier<NetworkEvent.Context>> messageConsumer) {
        if (NETWORK_CHANNEL == null)
            NETWORK_CHANNEL = NetworkRegistry.newSimpleChannel(new ResourceLocation(StarWars.modid, StarWars.modid), () -> "1.0", (s) -> true, (s) -> true);

        int id = networkId++;
        NETWORK_CHANNEL.registerMessage(id, messageType, encoder, decoder, messageConsumer);
        return id;
    }
}
