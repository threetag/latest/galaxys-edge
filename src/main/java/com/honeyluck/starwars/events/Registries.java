package com.honeyluck.starwars.events;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.client.models.sabers.*;
import com.honeyluck.starwars.common.block.BlockComponentConstructor;
import com.honeyluck.starwars.common.block.BlockKyberCrystal;
import com.honeyluck.starwars.common.block.BlockSaberConstructor;
import com.honeyluck.starwars.common.container.ContainerComponentConstructor;
import com.honeyluck.starwars.common.container.ContainerSaberConstructor;
import com.honeyluck.starwars.common.container.ContainerSaberConstructorModeConstruct;
import com.honeyluck.starwars.common.entity.EntityHolocron;
import com.honeyluck.starwars.common.item.*;
import com.honeyluck.starwars.common.tileentity.TileEntityComponentConstructor;
import com.honeyluck.starwars.common.tileentity.TileEntitySaberConstructor;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ObjectHolder;

import java.util.ArrayList;
import java.util.List;

@Mod.EventBusSubscriber(modid = StarWars.modid, bus = Mod.EventBusSubscriber.Bus.MOD)
public class Registries {
    public static List<Item> ITEMS = new ArrayList<>();
    public static List<Block> BLOCKS = new ArrayList<>();
    public static List<SoundEvent> SOUNDS = new ArrayList<>();
    public static List<EntityType> ENTITY_TYPES = new ArrayList<>();

    public static List<Item> STANDARD_HANDLE = new ArrayList<>();
    public static List<Item> STANDARD_POMMEL = new ArrayList<>();
    public static List<Item> STANDARD_EMITTER = new ArrayList<>();

    //Items
    public static Item skywalkerSaber = createItem(new ItemSaber("skywalker"), "sw_saber", STANDARD_HANDLE);
    public static Item skywalkerSaberPommel = createItem(new ItemSaberPart("skywalker", EnumSaberParts.POMMEL), "sw_saber_pommel", STANDARD_POMMEL);
    public static Item skywalkerSaberEmitter = createItem(new ItemSaberPart("skywalker", EnumSaberParts.EMITTER), "sw_saber_emitter", STANDARD_EMITTER);
    public static Item kyloSaber = createItem(new ItemSaber("kylo"), "kr_saber", STANDARD_HANDLE);
    public static Item kyloSaberPommel = createItem(new ItemSaberPart("kylo", EnumSaberParts.POMMEL), "kr_saber_pommel", STANDARD_POMMEL);
    public static Item kyloSaberEmitter = createItem(new ItemSaberPart("kylo", EnumSaberParts.CROSSGUARD), "kr_saber_emitter", STANDARD_EMITTER);
    public static Item winduSaber = createItem(new ItemSaber("windu"), "mw_saber", STANDARD_HANDLE);
    public static Item winduSaberPommel = createItem(new ItemSaberPart("windu", EnumSaberParts.POMMEL), "mw_saber_pommel", STANDARD_POMMEL);
    public static Item winduSaberEmitter = createItem(new ItemSaberPart("windu", EnumSaberParts.EMITTER), "mw_saber_emitter", STANDARD_EMITTER);
    public static Item crossSaber = createItem(new ItemSaber("cross"), "c_saber", STANDARD_HANDLE);
    public static Item crossSaberPommel = createItem(new ItemSaberPart("cross", EnumSaberParts.POMMEL), "c_saber_pommel", STANDARD_POMMEL);
    public static Item crossSaberEmitter = createItem(new ItemSaberPart("cross", EnumSaberParts.CROSSGUARD), "c_saber_emitter", STANDARD_EMITTER);
    public static Item blueprint = createItem(new ItemBlueprint(), "blueprint");
    public static Item handleBlueprint = createItem(new ItemBlueprint(), "handle_blueprint");
    public static Item emitterBlueprint = createItem(new ItemBlueprint(), "emitter_blueprint");
    public static Item pommelBlueprint = createItem(new ItemBlueprint(), "pommel_blueprint");
    public static Item powerCell = createItem(new Item(new Item.Properties().group(StarWars.tabItems)), "power_cell");
    public static Item focusingLens = createItem(new Item(new Item.Properties().group(StarWars.tabItems)), "focusing_lens");

    public static Item jediHolocron = createItem(new ItemHolocron(true, new Item.Properties().group(StarWars.tabItems).maxStackSize(1)), "jedi_holocron");
    public static Item sithHolocron = createItem(new ItemSithHolocron(), "sith_holocron");

    public static Item kyberCrystal = createItem(new ItemKyberCrystal(), "kyber_crystal");

    //Blocks
    public static Block saberConstructor = createBlock(new BlockSaberConstructor(), "saber_constructor", ItemGroup.DECORATIONS);
    public static Block kyberCrystals = createBlock(new BlockKyberCrystal(), "crystal");
    public static Block componentConstructor = createBlock(new BlockComponentConstructor(), "component_constructor", false);

    //Entities
    public static EntityType<?> holocronEntityType = EntityType.Builder.create(EntityHolocron::new, EntityClassification.MISC).size(0.25f, 0.25f).build("holocron").setRegistryName("holocron");

    //Tile Entity Types
    @ObjectHolder("galaxysedge:saber_constructor")
    public static TileEntityType<TileEntitySaberConstructor> saberConstructorType;

    @ObjectHolder("galaxysedge:component_constructor")
    public static TileEntityType<TileEntityComponentConstructor> componentConstructorType;

    //Containers
    @ObjectHolder("galaxysedge:saber_constructor")
    public static ContainerType<ContainerSaberConstructor> saberConstructorContainer;

    @ObjectHolder("galaxysedge:saber_constructor_mode_construct")
	public static ContainerType<ContainerSaberConstructorModeConstruct> saberConstructorModeConstructContainer;

    @ObjectHolder("galaxysedge:component_constructor")
    public static ContainerType<ContainerComponentConstructor> componentConstructorContainer;

    //Sounds
    public static SoundEvent saber_engage = createSoundEvent("saber_engage");
    public static SoundEvent saber_disengage = createSoundEvent("saber_disengage");
    public static SoundEvent saber_swing = createSoundEvent("saber_swing");

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)  {
        event.getRegistry().registerAll(ITEMS.toArray(new Item[ITEMS.size()]));
    }

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> e) {
        e.getRegistry().registerAll(BLOCKS.toArray(new Block[BLOCKS.size()]));
    }

    @SubscribeEvent
    public static void registerEntities(RegistryEvent.Register<EntityType<?>> e) {
        e.getRegistry().registerAll(
                holocronEntityType
        );
    }

    @SubscribeEvent
    public static void registerTileEntityTypes(RegistryEvent.Register<TileEntityType<?>> e) {
        e.getRegistry().register(TileEntityType.Builder.create(TileEntitySaberConstructor::new, saberConstructor).build(null).setRegistryName("saber_constructor"));
        e.getRegistry().register(TileEntityType.Builder.create(TileEntityComponentConstructor::new, componentConstructor).build(null).setRegistryName("component_constructor"));
    }

    @SubscribeEvent
    public static void registerContainers(RegistryEvent.Register<ContainerType<?>> e) {
        e.getRegistry().register(IForgeContainerType.create((windowId, inv, data) -> {
            BlockPos pos = data.readBlockPos();
            return new ContainerSaberConstructor(windowId, inv.player.world,pos, inv);
        }).setRegistryName("saber_constructor"));

	    e.getRegistry().register(IForgeContainerType.create((windowId, inv, data) -> {
		    BlockPos pos = data.readBlockPos();
		    return new ContainerSaberConstructorModeConstruct(windowId, inv.player.world,pos, inv);
	    }).setRegistryName("saber_constructor_mode_construct"));

        e.getRegistry().register(IForgeContainerType.create((windowId, inv, data) -> {
            BlockPos pos = data.readBlockPos();
            return new ContainerComponentConstructor(windowId, inv.player.world,pos, inv);
        }).setRegistryName("component_constructor"));
    }

    @SubscribeEvent
    public static void registerSounds(RegistryEvent.Register<SoundEvent> e) {
        e.getRegistry().registerAll(SOUNDS.toArray(new SoundEvent[SOUNDS.size()]));
    }

    public static Item createItem(Item item, String name) {
        item.setRegistryName(name);
        ITEMS.add(item);
        return item;
    }

    public static Item createItem(Item item, String name, List<Item> recipeGroup) {
        item.setRegistryName(name);
        ITEMS.add(item);
        recipeGroup.add(item);
        return item;
    }

    public static Block createBlock(Block block, String name) {
        block.setRegistryName(name);
        BLOCKS.add(block);
        ITEMS.add(new BlockItem(block, new Item.Properties()).setRegistryName(name));
        return block;
    }

    public static Block createBlock(Block block, String name, boolean createItem) {
        block.setRegistryName(name);
        BLOCKS.add(block);
        if(createItem) ITEMS.add(new BlockItem(block, new Item.Properties()).setRegistryName(name));
        return block;
    }

    public static Block createBlock(Block block, String name, ItemGroup group) {
        block.setRegistryName(name);
        BLOCKS.add(block);
        ITEMS.add(new BlockItem(block, new Item.Properties().group(group)).setRegistryName(name));
        return block;
    }
    
    public static SoundEvent createSoundEvent(String name) {
        SoundEvent sound = new SoundEvent(new ResourceLocation(StarWars.modid, name)).setRegistryName(StarWars.modid, name);
        SOUNDS.add(sound);
        return sound;
    }
}
