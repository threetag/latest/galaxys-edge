package com.honeyluck.starwars.client.render.layer;

import com.honeyluck.starwars.common.item.ItemSaber;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.CapabilityItemHandler;

public class RenderLayerLightsaber extends LayerRenderer {

    private Minecraft minecraft;
    private PlayerRenderer playerRenderer;
    private int count;

    public RenderLayerLightsaber(IEntityRenderer entityRendererIn) {
        super(entityRendererIn);
        minecraft = Minecraft.getInstance();
        this.playerRenderer = (PlayerRenderer)entityRendererIn;
    }

    @Override
    public void render(Entity entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
        if(entity instanceof PlayerEntity){
            count = 0;
            PlayerEntity player = (PlayerEntity) entity;
            for(int i = 0; i <= player.inventory.getSizeInventory(); i++) {
                ItemStack stack = player.inventory.getStackInSlot(i);
                if(player.inventory.getStackInSlot(i).getItem() instanceof ItemSaber && stack != player.inventory.getCurrentItem() && stack != player.getHeldItemOffhand()) {
                    ItemStack itemSaber = player.inventory.getStackInSlot(i);
                    itemSaber.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(itemHandler -> {
                        if(count < 2) {
                            if(!itemHandler.getStackInSlot(0).isEmpty() && !itemHandler.getStackInSlot(1).isEmpty() && !itemHandler.getStackInSlot(2).isEmpty()) {
                                count += 1;
                                GlStateManager.pushMatrix();
                                GlStateManager.rotated(180, 1, 0, 0);
                                if(player.isSneaking()) {
                                    GlStateManager.rotated(30, 1, 0, 0);
                                    GlStateManager.translated(0, 0, 0.15);
                                }
                                if(count == 1) {
                                    GlStateManager.translated(-0.275, -0.8, 0.1);
                                }
                                else {
                                    GlStateManager.translated(0.275, -0.8, 0.1);
                                }
                                minecraft.getItemRenderer().renderItem(itemSaber, ItemCameraTransforms.TransformType.FIXED);
                                GlStateManager.popMatrix();
                            }
                        }
                    });

                }
            }
        }
    }

    @Override
    public boolean shouldCombineTextures() {
        return false;
    }
}
