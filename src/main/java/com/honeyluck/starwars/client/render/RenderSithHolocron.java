package com.honeyluck.starwars.client.render;


import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.client.models.item.SithHolocron;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.tileentity.ItemStackTileEntityRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class RenderSithHolocron extends ItemStackTileEntityRenderer {

    public ResourceLocation TEX = new ResourceLocation(StarWars.modid, "textures/items/sith_holocron.png");
    public SithHolocron holocronModel = new SithHolocron();
    public Minecraft mc = Minecraft.getInstance();


    @Override
    public void renderByItem(ItemStack itemStackIn) {
        GlStateManager.pushMatrix();
        GlStateManager.translated(8*0.0625, 1, 8*0.0625f);
        GlStateManager.rotated(180, 0, 0, 1);
        GlStateManager.rotated(180, 0, 1, 0);
        mc.getTextureManager().bindTexture(TEX);
        if(itemStackIn.getOrCreateTag().getBoolean("open")) {
            GlStateManager.disableLighting();
        }
        //GlStateManager.scaled(0.25, 0.25, 0.25);
        holocronModel.render(0.0625f);
        GlStateManager.popMatrix();
    }
}
