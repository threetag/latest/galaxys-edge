package com.honeyluck.starwars.client.render;

import com.honeyluck.starwars.client.gui.SaberConstructorScreen;
import com.honeyluck.starwars.common.item.ItemKyberCrystal;
import com.honeyluck.starwars.common.tileentity.TileEntityComponentConstructor;
import com.honeyluck.starwars.common.tileentity.TileEntitySaberConstructor;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class RenderComponentConstructor extends TileEntityRenderer<TileEntityComponentConstructor>
{
    @Override
    public void render(TileEntityComponentConstructor tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage) {
        //		super.render(tileEntityIn, x, y, z, partialTicks, destroyStage);
        GlStateManager.pushMatrix();
        GlStateManager.translated(x, y, z);
        switch (tileEntityIn.getWorld().getBlockState(tileEntityIn.getPos()).get(BlockStateProperties.HORIZONTAL_FACING))
        {
            case DOWN:
                break;
            case UP:
                break;
            case NORTH:
                GlStateManager.rotatef(180f, 0, 1f, 0);
                GlStateManager.translatef(-1f, 0f, -1f);
                break;
            case SOUTH:
                break;
            case WEST:
                GlStateManager.rotatef(-90f, 0, 1f, 0);
                GlStateManager.translatef(0f, 0f, -1f);
                break;
            case EAST:
                GlStateManager.rotatef(90f, 0, 1f, 0);
                GlStateManager.translatef(-1f, 0f, 0f);
                break;
        }

        GlStateManager.pushMatrix();
        GlStateManager.translated(0.5, 1.1, 0.5);
        if(tileEntityIn.getInventory().getStackInSlot(4).getItem() instanceof ItemKyberCrystal) {
            GlStateManager.scaled(0.5, 0.5, 0.5);
            GlStateManager.translated(0, -0.15, 0.25);
            GlStateManager.rotated(-90, 1, 0, 0);
            GlStateManager.rotated(22.5, 0, 0, 1);

        } else {
            GlStateManager.rotated(22.5, 0, 1, 0);
        }
        GlStateManager.translated(-0.125, 0, 0);
        Minecraft.getInstance().getItemRenderer().renderItem(tileEntityIn.getInventory().getStackInSlot(4), ItemCameraTransforms.TransformType.GROUND);
        GlStateManager.popMatrix();
        GlStateManager.popMatrix();
    }
}
