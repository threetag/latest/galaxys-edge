package com.honeyluck.starwars.client.models.sabers;

import com.honeyluck.starwars.StarWars;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.util.ResourceLocation;

public class ModelSaber extends Model {

    public RendererModel lightsaber;
    public ResourceLocation TEX;
    public Minecraft mc = Minecraft.getInstance();
    public void renderHandle(float f5) {}
    public void renderEmitter(float f5) {}
    public void renderPommel(float f5) {}
    public void render(float f5) {}
    public void bindTex() {
        mc.getTextureManager().bindTexture(TEX);
    }
    public float getHeight() { //Handle Height
        return 0;
    }
    public float getEmitterHeight() { //Emitter Height (from base to where the blade starts)
        return 0;
    }

    public float getCrossGuardOffset() { //Height difference between emitter height and where to position the cross blades
        return 0;
    }

}
