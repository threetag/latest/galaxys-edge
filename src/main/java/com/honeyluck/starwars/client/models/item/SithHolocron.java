package com.honeyluck.starwars.client.models.item;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;

//Model By Violet

public class SithHolocron extends EntityModel {
	private final RendererModel Pyramid;
	private final RendererModel Slope1;
	private final RendererModel Slope2;
	private final RendererModel Slope3;
	private final RendererModel Slope4;
	private final RendererModel BottomFormers1;
	private final RendererModel BottomFormers2;
	private final RendererModel BottomFormers3;
	private final RendererModel BottomFormers4;
	private final RendererModel FaceC1;
	private final RendererModel FaceA1;
	private final RendererModel LA;
	private final RendererModel LA2;
	private final RendererModel MB1;
	private final RendererModel RA1;
	private final RendererModel RA2;
	private final RendererModel MB2;
	private final RendererModel FaceC2;
	private final RendererModel FaceA2;
	private final RendererModel LA3;
	private final RendererModel LA4;
	private final RendererModel MB3;
	private final RendererModel RA3;
	private final RendererModel RA4;
	private final RendererModel MB4;
	private final RendererModel FaceC3;
	private final RendererModel FaceA3;
	private final RendererModel LA5;
	private final RendererModel LA6;
	private final RendererModel MB5;
	private final RendererModel RA5;
	private final RendererModel RA6;
	private final RendererModel MB6;
	private final RendererModel FaceC4;
	private final RendererModel FaceA4;
	private final RendererModel LA7;
	private final RendererModel LA8;
	private final RendererModel MB7;
	private final RendererModel RA7;
	private final RendererModel RA8;
	private final RendererModel MB8;

	public SithHolocron() {
		textureWidth = 64;
		textureHeight = 64;

		Pyramid = new RendererModel(this);
		Pyramid.setRotationPoint(0.0F, 24.0F, 0.0F);
		Pyramid.cubeList.add(new ModelBox(Pyramid, 18, 4, -0.5F, -14.25F, -0.5F, 1, 1, 1, 0.0F, false));
		Pyramid.cubeList.add(new ModelBox(Pyramid, 0, 17, -8.0F, 0.0F, -0.5F, 16, 1, 1, 0.0F, false));
		Pyramid.cubeList.add(new ModelBox(Pyramid, 0, 0, -0.5F, 0.0F, -8.0F, 1, 1, 16, 0.0F, false));

		Slope1 = new RendererModel(this);
		Slope1.setRotationPoint(0.0F, 0.0F, -8.0F);
		Pyramid.addChild(Slope1);
		setRotationAngle(Slope1, -0.4887F, 0.0F, 0.0F);
		Slope1.cubeList.add(new ModelBox(Slope1, 8, 40, -0.5F, -16.0F, 0.0F, 1, 16, 1, 0.0F, false));

		Slope2 = new RendererModel(this);
		Slope2.setRotationPoint(0.0F, 0.0F, 8.0F);
		Pyramid.addChild(Slope2);
		setRotationAngle(Slope2, 0.4887F, 0.0F, 0.0F);
		Slope2.cubeList.add(new ModelBox(Slope2, 8, 40, -0.5F, -16.0F, -1.0F, 1, 16, 1, 0.0F, false));

		Slope3 = new RendererModel(this);
		Slope3.setRotationPoint(8.0F, 0.0F, 0.0F);
		Pyramid.addChild(Slope3);
		setRotationAngle(Slope3, 0.0F, 0.0F, -0.4887F);
		Slope3.cubeList.add(new ModelBox(Slope3, 8, 40, -1.0F, -16.0F, -0.5F, 1, 16, 1, 0.0F, false));

		Slope4 = new RendererModel(this);
		Slope4.setRotationPoint(-8.0F, 0.0F, 0.0F);
		Pyramid.addChild(Slope4);
		setRotationAngle(Slope4, 0.0F, 0.0F, 0.4887F);
		Slope4.cubeList.add(new ModelBox(Slope4, 8, 40, 0.0F, -16.0F, -0.5F, 1, 16, 1, 0.0F, false));

		BottomFormers1 = new RendererModel(this);
		BottomFormers1.setRotationPoint(0.0F, 0.0F, 0.0F);
		Pyramid.addChild(BottomFormers1);
		setRotationAngle(BottomFormers1, 0.0F, -0.7854F, 0.0F);
		BottomFormers1.cubeList.add(new ModelBox(BottomFormers1, 0, 26, -0.6967F, 0.0F, -6.0104F, 6, 1, 6, 0.0F, false));
		BottomFormers1.cubeList.add(new ModelBox(BottomFormers1, 0, 19, -5.3033F, 0.0F, -6.0104F, 6, 1, 6, 0.0F, false));

		BottomFormers2 = new RendererModel(this);
		BottomFormers2.setRotationPoint(0.0F, 0.0F, 0.0F);
		Pyramid.addChild(BottomFormers2);
		setRotationAngle(BottomFormers2, 0.0F, -2.3562F, 0.0F);
		BottomFormers2.cubeList.add(new ModelBox(BottomFormers2, 0, 26, -0.6967F, 0.0F, -6.0104F, 6, 1, 6, 0.0F, false));
		BottomFormers2.cubeList.add(new ModelBox(BottomFormers2, 0, 19, -5.3033F, 0.0F, -6.0104F, 6, 1, 6, 0.0F, false));

		BottomFormers3 = new RendererModel(this);
		BottomFormers3.setRotationPoint(0.0F, 0.0F, 0.0F);
		Pyramid.addChild(BottomFormers3);
		setRotationAngle(BottomFormers3, 0.0F, 2.3562F, 0.0F);
		BottomFormers3.cubeList.add(new ModelBox(BottomFormers3, 0, 26, -0.6967F, 0.0F, -6.0104F, 6, 1, 6, 0.0F, false));
		BottomFormers3.cubeList.add(new ModelBox(BottomFormers3, 0, 19, -5.3033F, 0.0F, -6.0104F, 6, 1, 6, 0.0F, false));

		BottomFormers4 = new RendererModel(this);
		BottomFormers4.setRotationPoint(0.0F, 0.0F, 0.0F);
		Pyramid.addChild(BottomFormers4);
		setRotationAngle(BottomFormers4, 0.0F, 0.7854F, 0.0F);
		BottomFormers4.cubeList.add(new ModelBox(BottomFormers4, 0, 26, -0.6967F, 0.0F, -6.0104F, 6, 1, 6, 0.0F, false));
		BottomFormers4.cubeList.add(new ModelBox(BottomFormers4, 0, 19, -5.3033F, 0.0F, -6.0104F, 6, 1, 6, 0.0F, false));

		FaceC1 = new RendererModel(this);
		FaceC1.setRotationPoint(0.0F, 0.0F, 0.0F);
		Pyramid.addChild(FaceC1);
		setRotationAngle(FaceC1, 0.0F, -0.7854F, 0.0F);
		

		FaceA1 = new RendererModel(this);
		FaceA1.setRotationPoint(0.0F, 0.0F, -5.75F);
		FaceC1.addChild(FaceA1);
		setRotationAngle(FaceA1, -0.3665F, 0.0F, 0.0F);
		FaceA1.cubeList.add(new ModelBox(FaceA1, 46, 60, -0.875F, -1.0F, 0.0F, 6, 1, 1, 0.0F, false));
		FaceA1.cubeList.add(new ModelBox(FaceA1, 46, 58, -5.125F, -1.0F, 0.0F, 6, 1, 1, 0.0F, false));
		FaceA1.cubeList.add(new ModelBox(FaceA1, 52, 49, -2.0F, -9.0F, 0.0F, 4, 1, 1, 0.0F, false));
		FaceA1.cubeList.add(new ModelBox(FaceA1, 36, 19, -4.0F, -5.0F, 0.25F, 8, 4, 1, 0.0F, false));
		FaceA1.cubeList.add(new ModelBox(FaceA1, 44, 44, -2.5F, -9.0F, 0.25F, 5, 4, 1, 0.0F, false));
		FaceA1.cubeList.add(new ModelBox(FaceA1, 32, 46, -1.5F, -12.0F, 0.25F, 3, 3, 1, 0.0F, false));

		LA = new RendererModel(this);
		LA.setRotationPoint(4.75F, -1.0F, 0.0F);
		FaceA1.addChild(LA);
		setRotationAngle(LA, 0.0F, 0.0F, -0.3491F);
		LA.cubeList.add(new ModelBox(LA, 24, 41, -0.75F, -14.0F, 0.0F, 1, 14, 1, 0.0F, false));

		LA2 = new RendererModel(this);
		LA2.setRotationPoint(-0.75F, -3.0F, 0.0F);
		LA.addChild(LA2);
		setRotationAngle(LA2, 0.0F, 0.0F, 0.6981F);
		LA2.cubeList.add(new ModelBox(LA2, 0, 33, 0.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F, false));

		MB1 = new RendererModel(this);
		MB1.setRotationPoint(0.0F, 2.0F, 0.0F);
		LA2.addChild(MB1);
		setRotationAngle(MB1, 0.0F, 0.0F, -0.7679F);
		MB1.cubeList.add(new ModelBox(MB1, 15, 56, 0.0F, -7.0F, 0.0F, 1, 7, 1, 0.0F, false));

		RA1 = new RendererModel(this);
		RA1.setRotationPoint(-4.75F, -1.0F, 0.0F);
		FaceA1.addChild(RA1);
		setRotationAngle(RA1, 0.0F, 0.0F, 0.3491F);
		RA1.cubeList.add(new ModelBox(RA1, 24, 41, -0.25F, -14.0F, 0.0F, 1, 14, 1, 0.0F, false));

		RA2 = new RendererModel(this);
		RA2.setRotationPoint(0.75F, -3.0F, 0.0F);
		RA1.addChild(RA2);
		setRotationAngle(RA2, 0.0F, 0.0F, -0.6981F);
		RA2.cubeList.add(new ModelBox(RA2, 0, 33, -1.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F, false));

		MB2 = new RendererModel(this);
		MB2.setRotationPoint(0.0F, 2.0F, 0.0F);
		RA2.addChild(MB2);
		setRotationAngle(MB2, 0.0F, 0.0F, 0.7679F);
		MB2.cubeList.add(new ModelBox(MB2, 15, 56, -1.0F, -7.0F, 0.0F, 1, 7, 1, 0.0F, false));

		FaceC2 = new RendererModel(this);
		FaceC2.setRotationPoint(0.0F, 0.0F, 0.0F);
		Pyramid.addChild(FaceC2);
		setRotationAngle(FaceC2, 0.0F, -2.3562F, 0.0F);
		

		FaceA2 = new RendererModel(this);
		FaceA2.setRotationPoint(0.0F, 0.0F, -5.75F);
		FaceC2.addChild(FaceA2);
		setRotationAngle(FaceA2, -0.3665F, 0.0F, 0.0F);
		FaceA2.cubeList.add(new ModelBox(FaceA2, 46, 60, -0.875F, -1.0F, 0.0F, 6, 1, 1, 0.0F, false));
		FaceA2.cubeList.add(new ModelBox(FaceA2, 46, 58, -5.125F, -1.0F, 0.0F, 6, 1, 1, 0.0F, false));
		FaceA2.cubeList.add(new ModelBox(FaceA2, 52, 49, -2.0F, -9.0F, 0.0F, 4, 1, 1, 0.0F, false));
		FaceA2.cubeList.add(new ModelBox(FaceA2, 36, 19, -4.0F, -5.0F, 0.25F, 8, 4, 1, 0.0F, false));
		FaceA2.cubeList.add(new ModelBox(FaceA2, 44, 44, -2.5F, -9.0F, 0.25F, 5, 4, 1, 0.0F, false));
		FaceA2.cubeList.add(new ModelBox(FaceA2, 32, 46, -1.5F, -12.0F, 0.25F, 3, 3, 1, 0.0F, false));

		LA3 = new RendererModel(this);
		LA3.setRotationPoint(4.75F, -1.0F, 0.0F);
		FaceA2.addChild(LA3);
		setRotationAngle(LA3, 0.0F, 0.0F, -0.3491F);
		LA3.cubeList.add(new ModelBox(LA3, 24, 41, -0.75F, -14.0F, 0.0F, 1, 14, 1, 0.0F, false));

		LA4 = new RendererModel(this);
		LA4.setRotationPoint(-0.75F, -3.0F, 0.0F);
		LA3.addChild(LA4);
		setRotationAngle(LA4, 0.0F, 0.0F, 0.6981F);
		LA4.cubeList.add(new ModelBox(LA4, 0, 33, 0.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F, false));

		MB3 = new RendererModel(this);
		MB3.setRotationPoint(0.0F, 2.0F, 0.0F);
		LA4.addChild(MB3);
		setRotationAngle(MB3, 0.0F, 0.0F, -0.7679F);
		MB3.cubeList.add(new ModelBox(MB3, 15, 56, 0.0F, -7.0F, 0.0F, 1, 7, 1, 0.0F, false));

		RA3 = new RendererModel(this);
		RA3.setRotationPoint(-4.75F, -1.0F, 0.0F);
		FaceA2.addChild(RA3);
		setRotationAngle(RA3, 0.0F, 0.0F, 0.3491F);
		RA3.cubeList.add(new ModelBox(RA3, 24, 41, -0.25F, -14.0F, 0.0F, 1, 14, 1, 0.0F, false));

		RA4 = new RendererModel(this);
		RA4.setRotationPoint(0.75F, -3.0F, 0.0F);
		RA3.addChild(RA4);
		setRotationAngle(RA4, 0.0F, 0.0F, -0.6981F);
		RA4.cubeList.add(new ModelBox(RA4, 0, 33, -1.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F, false));

		MB4 = new RendererModel(this);
		MB4.setRotationPoint(0.0F, 2.0F, 0.0F);
		RA4.addChild(MB4);
		setRotationAngle(MB4, 0.0F, 0.0F, 0.7679F);
		MB4.cubeList.add(new ModelBox(MB4, 15, 56, -1.0F, -7.0F, 0.0F, 1, 7, 1, 0.0F, false));

		FaceC3 = new RendererModel(this);
		FaceC3.setRotationPoint(0.0F, 0.0F, 0.0F);
		Pyramid.addChild(FaceC3);
		setRotationAngle(FaceC3, 0.0F, 2.3562F, 0.0F);
		

		FaceA3 = new RendererModel(this);
		FaceA3.setRotationPoint(0.0F, 0.0F, -5.75F);
		FaceC3.addChild(FaceA3);
		setRotationAngle(FaceA3, -0.3665F, 0.0F, 0.0F);
		FaceA3.cubeList.add(new ModelBox(FaceA3, 46, 60, -0.875F, -1.0F, 0.0F, 6, 1, 1, 0.0F, false));
		FaceA3.cubeList.add(new ModelBox(FaceA3, 46, 58, -5.125F, -1.0F, 0.0F, 6, 1, 1, 0.0F, false));
		FaceA3.cubeList.add(new ModelBox(FaceA3, 52, 49, -2.0F, -9.0F, 0.0F, 4, 1, 1, 0.0F, false));
		FaceA3.cubeList.add(new ModelBox(FaceA3, 36, 19, -4.0F, -5.0F, 0.25F, 8, 4, 1, 0.0F, false));
		FaceA3.cubeList.add(new ModelBox(FaceA3, 44, 44, -2.5F, -9.0F, 0.25F, 5, 4, 1, 0.0F, false));
		FaceA3.cubeList.add(new ModelBox(FaceA3, 32, 46, -1.5F, -12.0F, 0.25F, 3, 3, 1, 0.0F, false));

		LA5 = new RendererModel(this);
		LA5.setRotationPoint(4.75F, -1.0F, 0.0F);
		FaceA3.addChild(LA5);
		setRotationAngle(LA5, 0.0F, 0.0F, -0.3491F);
		LA5.cubeList.add(new ModelBox(LA5, 24, 41, -0.75F, -14.0F, 0.0F, 1, 14, 1, 0.0F, false));

		LA6 = new RendererModel(this);
		LA6.setRotationPoint(-0.75F, -3.0F, 0.0F);
		LA5.addChild(LA6);
		setRotationAngle(LA6, 0.0F, 0.0F, 0.6981F);
		LA6.cubeList.add(new ModelBox(LA6, 0, 33, 0.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F, false));

		MB5 = new RendererModel(this);
		MB5.setRotationPoint(0.0F, 2.0F, 0.0F);
		LA6.addChild(MB5);
		setRotationAngle(MB5, 0.0F, 0.0F, -0.7679F);
		MB5.cubeList.add(new ModelBox(MB5, 15, 56, 0.0F, -7.0F, 0.0F, 1, 7, 1, 0.0F, false));

		RA5 = new RendererModel(this);
		RA5.setRotationPoint(-4.75F, -1.0F, 0.0F);
		FaceA3.addChild(RA5);
		setRotationAngle(RA5, 0.0F, 0.0F, 0.3491F);
		RA5.cubeList.add(new ModelBox(RA5, 24, 41, -0.25F, -14.0F, 0.0F, 1, 14, 1, 0.0F, false));

		RA6 = new RendererModel(this);
		RA6.setRotationPoint(0.75F, -3.0F, 0.0F);
		RA5.addChild(RA6);
		setRotationAngle(RA6, 0.0F, 0.0F, -0.6981F);
		RA6.cubeList.add(new ModelBox(RA6, 0, 33, -1.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F, false));

		MB6 = new RendererModel(this);
		MB6.setRotationPoint(0.0F, 2.0F, 0.0F);
		RA6.addChild(MB6);
		setRotationAngle(MB6, 0.0F, 0.0F, 0.7679F);
		MB6.cubeList.add(new ModelBox(MB6, 15, 56, -1.0F, -7.0F, 0.0F, 1, 7, 1, 0.0F, false));

		FaceC4 = new RendererModel(this);
		FaceC4.setRotationPoint(0.0F, 0.0F, 0.0F);
		Pyramid.addChild(FaceC4);
		setRotationAngle(FaceC4, 0.0F, 0.7854F, 0.0F);
		

		FaceA4 = new RendererModel(this);
		FaceA4.setRotationPoint(0.0F, 0.0F, -5.75F);
		FaceC4.addChild(FaceA4);
		setRotationAngle(FaceA4, -0.3665F, 0.0F, 0.0F);
		FaceA4.cubeList.add(new ModelBox(FaceA4, 46, 60, -0.875F, -1.0F, 0.0F, 6, 1, 1, 0.0F, false));
		FaceA4.cubeList.add(new ModelBox(FaceA4, 46, 58, -5.125F, -1.0F, 0.0F, 6, 1, 1, 0.0F, false));
		FaceA4.cubeList.add(new ModelBox(FaceA4, 52, 49, -2.0F, -9.0F, 0.0F, 4, 1, 1, 0.0F, false));
		FaceA4.cubeList.add(new ModelBox(FaceA4, 36, 19, -4.0F, -5.0F, 0.25F, 8, 4, 1, 0.0F, false));
		FaceA4.cubeList.add(new ModelBox(FaceA4, 44, 44, -2.5F, -9.0F, 0.25F, 5, 4, 1, 0.0F, false));
		FaceA4.cubeList.add(new ModelBox(FaceA4, 32, 46, -1.5F, -12.0F, 0.25F, 3, 3, 1, 0.0F, false));

		LA7 = new RendererModel(this);
		LA7.setRotationPoint(4.75F, -1.0F, 0.0F);
		FaceA4.addChild(LA7);
		setRotationAngle(LA7, 0.0F, 0.0F, -0.3491F);
		LA7.cubeList.add(new ModelBox(LA7, 24, 41, -0.75F, -14.0F, 0.0F, 1, 14, 1, 0.0F, false));

		LA8 = new RendererModel(this);
		LA8.setRotationPoint(-0.75F, -3.0F, 0.0F);
		LA7.addChild(LA8);
		setRotationAngle(LA8, 0.0F, 0.0F, 0.6981F);
		LA8.cubeList.add(new ModelBox(LA8, 0, 33, 0.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F, false));

		MB7 = new RendererModel(this);
		MB7.setRotationPoint(0.0F, 2.0F, 0.0F);
		LA8.addChild(MB7);
		setRotationAngle(MB7, 0.0F, 0.0F, -0.7679F);
		MB7.cubeList.add(new ModelBox(MB7, 15, 56, 0.0F, -7.0F, 0.0F, 1, 7, 1, 0.0F, false));

		RA7 = new RendererModel(this);
		RA7.setRotationPoint(-4.75F, -1.0F, 0.0F);
		FaceA4.addChild(RA7);
		setRotationAngle(RA7, 0.0F, 0.0F, 0.3491F);
		RA7.cubeList.add(new ModelBox(RA7, 24, 41, -0.25F, -14.0F, 0.0F, 1, 14, 1, 0.0F, false));

		RA8 = new RendererModel(this);
		RA8.setRotationPoint(0.75F, -3.0F, 0.0F);
		RA7.addChild(RA8);
		setRotationAngle(RA8, 0.0F, 0.0F, -0.6981F);
		RA8.cubeList.add(new ModelBox(RA8, 0, 33, -1.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F, false));

		MB8 = new RendererModel(this);
		MB8.setRotationPoint(0.0F, 2.0F, 0.0F);
		RA8.addChild(MB8);
		setRotationAngle(MB8, 0.0F, 0.0F, 0.7679F);
		MB8.cubeList.add(new ModelBox(MB8, 15, 56, -1.0F, -7.0F, 0.0F, 1, 7, 1, 0.0F, false));
	}

	public void render(float f5) {
		Pyramid.render(f5);
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}