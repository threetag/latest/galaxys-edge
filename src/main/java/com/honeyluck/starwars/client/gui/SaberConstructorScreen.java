package com.honeyluck.starwars.client.gui;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.common.container.AbstractSaberConstructorContainer;
import com.honeyluck.starwars.common.container.ContainerSaberConstructor;
import com.honeyluck.starwars.common.container.ContainerSaberConstructorModeConstruct;
import com.honeyluck.starwars.network.MessageUpdateSaberConstructor;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.client.config.GuiButtonExt;
import net.minecraftforge.fml.network.PacketDistributor;

public class SaberConstructorScreen<T extends AbstractSaberConstructorContainer> extends ContainerScreen<T> {
    private ResourceLocation LIGHTSABER_TEXTURE = new ResourceLocation(StarWars.modid, "textures/gui/saberconstructor.png");
    private ResourceLocation EMPTY_TEXTURE = new ResourceLocation(StarWars.modid, "textures/gui/saberconstructor_0.png");

    private Button confirm;
    private SaberConstructorMode mode;

    public SaberConstructorScreen(T container, PlayerInventory inventory, ITextComponent name, SaberConstructorMode saberConstructorMode) {
        super(container, inventory, name);
        this.xSize = 178;
        this.ySize = 198;
        this.mode = saberConstructorMode;
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground();
        super.render(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void init() {
        super.init();
        int x = (this.width - xSize) / 2;
        int y = (this.height - ySize) / 2;

	    SaberConstructorMode modeToChange = this.mode == SaberConstructorMode.MODIFY ? SaberConstructorMode.CONSTRUCT : SaberConstructorMode.MODIFY;
        String btnName = new TranslationTextComponent(StarWars.modid + ".button." + modeToChange.toString().toLowerCase()).getFormattedText();
        int buttonWidth = font.getStringWidth(btnName) + 10;
        confirm = new GuiButtonExt(x + (this.xSize/2) - buttonWidth/2, y + 42, buttonWidth, 12, btnName, (button) -> {
            //Packet needed here to change GUI - not sure what I'm doing though
	        StarWars.NETWORK_CHANNEL.send(PacketDistributor.SERVER.noArg(), new MessageUpdateSaberConstructor(modeToChange));
        });
        this.addButton(confirm);
    }

    @Override
    public void tick() {
        super.tick();
        if(this.getContainer().getInventory().get(18).isEmpty()) {
            confirm.active = false;
        } else {
            confirm.active = true;
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        this.font.drawString(this.title.getUnformattedComponentText(), 8.0F, 4, 4210752);
        this.font.drawString(this.playerInventory.getDisplayName().getFormattedText(), 8.0F, 100, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1, 1, 1, 1);
        if(this.mode == SaberConstructorMode.CONSTRUCT) {
            this.minecraft.getTextureManager().bindTexture(EMPTY_TEXTURE);
        } else {
            this.minecraft.getTextureManager().bindTexture(LIGHTSABER_TEXTURE);
        }

        int xPos = (this.width - this.xSize) / 2;
        int yPos = (this.height - this.ySize) / 2;
        this.blit(xPos, yPos, 0, 0, this.xSize, this.ySize);
    }

    public enum SaberConstructorMode {
    	MODIFY, CONSTRUCT
    }

    public static class ModifyScreen extends SaberConstructorScreen<ContainerSaberConstructor> {

	    public ModifyScreen(ContainerSaberConstructor container, PlayerInventory inventory, ITextComponent name) {
		    super(container, inventory, name, SaberConstructorMode.MODIFY);
	    }
    }

	public static class ConstructScreen extends SaberConstructorScreen<ContainerSaberConstructorModeConstruct> {

		public ConstructScreen(ContainerSaberConstructorModeConstruct container, PlayerInventory inventory, ITextComponent name) {
			super(container, inventory, name, SaberConstructorMode.CONSTRUCT);
		}
	}
}
