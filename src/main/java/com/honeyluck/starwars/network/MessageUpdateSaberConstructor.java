package com.honeyluck.starwars.network;

import com.honeyluck.starwars.client.gui.SaberConstructorScreen;
import com.honeyluck.starwars.common.container.AbstractSaberConstructorContainer;
import com.honeyluck.starwars.common.tileentity.TileEntitySaberConstructor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.NetworkHooks;

import java.util.function.Supplier;

public class MessageUpdateSaberConstructor {
    public SaberConstructorScreen.SaberConstructorMode mode;

    public MessageUpdateSaberConstructor(SaberConstructorScreen.SaberConstructorMode mode) {
        this.mode = mode;
    }

    public MessageUpdateSaberConstructor(PacketBuffer buffer) {
        this.mode = SaberConstructorScreen.SaberConstructorMode.values()[buffer.readInt()];
    }

    public void toBytes(PacketBuffer buffer) {
        buffer.writeInt(this.mode.ordinal());
    }

    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            PlayerEntity player = ctx.get().getSender();
            if(player != null) {
                if(player.openContainer instanceof AbstractSaberConstructorContainer) {
	                AbstractSaberConstructorContainer container = (AbstractSaberConstructorContainer)player.openContainer;
	                if (container.getTileEntity() instanceof TileEntitySaberConstructor) {
	                	TileEntitySaberConstructor te = (TileEntitySaberConstructor) container.getTileEntity();
	                	te.setSaberConstructorMode(this.mode);
		                NetworkHooks.openGui((ServerPlayerEntity) player, te, te.getPos());
	                }
                }
            }
        });
        ctx.get().setPacketHandled(true);
    }
}
