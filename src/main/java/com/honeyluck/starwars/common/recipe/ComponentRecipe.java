package com.honeyluck.starwars.common.recipe;

import com.google.common.collect.Lists;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ComponentRecipe {

    private Item output;
    private List<Item> inputs = new ArrayList<>();

    public ComponentRecipe(Item output, Item... inputs) {
        this.output = output;
        for(Item item : inputs) {
            this.inputs.add(item);
        }
    }

    public Item getOutput() {
        return output;
    }

    public List<Item> getInputs() {
        return inputs;
    }

    public boolean matches(ItemStack... recipe) {
        List<Item> playerInputs = new ArrayList<>();

        for(ItemStack stack : recipe) {
            if(!stack.isEmpty()){
                playerInputs.add(stack.getItem());
            }
        }

        if(!playerInputs.equals(inputs)) return false;

        if(!inputs.equals(playerInputs)) return false;

        return true;
    }
}
