package com.honeyluck.starwars.common.tileentity;

import com.honeyluck.starwars.common.container.ContainerComponentConstructor;
import com.honeyluck.starwars.events.Registries;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class TileEntityComponentConstructor extends TileEntity implements ITickableTileEntity, INamedContainerProvider {


    private ItemStackHandler inventory = new ItemStackHandler(5) {
        @Override
        public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
            if(slot == 4) {
                return false;
            }
            return true;
        }

        @Nonnull
        @Override
        public ItemStack extractItem(int slot, int amount, boolean simulate) {
            if(slot < 4) {
                inventory.setStackInSlot(4, ItemStack.EMPTY); //Empty result slot when ingredients are removed
            }
            return super.extractItem(slot, amount, simulate);
        }

    };

    public TileEntityComponentConstructor() {
        super(Registries.componentConstructorType);
    }

    @Override
    public ITextComponent getDisplayName() {
        ResourceLocation regName = getType().getRegistryName();
        return new TranslationTextComponent("container." + regName.getNamespace() + "." + regName.getPath());
    }

    @Nullable
    @Override
    public Container createMenu(int i, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return new ContainerComponentConstructor(i, world, pos, playerInventory);
    }

    @Override
    public void tick() {

    }

    @Override
    public void read(CompoundNBT compound) {
        CompoundNBT nbt = compound.getCompound("inventory");
        inventory.deserializeNBT(nbt);
        super.read(compound);
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        CompoundNBT nbt = inventory.serializeNBT();
        compound.put("inventory", nbt);
        return super.write(compound);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if(cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return LazyOptional.of(() -> (T) this.inventory);
        }
        return super.getCapability(cap, side);
    }

    @Nullable
    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        return new SUpdateTileEntityPacket(getPos(), 1, getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
        this.handleUpdateTag(pkt.getNbtCompound());
    }

    public ItemStackHandler getInventory() {
        return inventory;
    }
}
