package com.honeyluck.starwars.common.utils;

import com.honeyluck.starwars.client.models.sabers.*;
import com.mojang.blaze3d.platform.GLX;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.event.TickEvent;

import java.util.HashMap;

@OnlyIn(Dist.CLIENT)
public class RenderUtils {

    public static float renderTickTime;
    private static float lastBrightnessX = GLX.lastBrightnessX;
    private static float lastBrightnessY = GLX.lastBrightnessY;

    public static HashMap<String, ModelSaber> saberModels = new HashMap<String, ModelSaber>() {{
        put("skywalker", new SkywalkerSaber());
        put("kylo", new KyloSaber());
        put("cross", new CrossSaber());
        put("windu", new WinduSaber());
        put("kenobi", new KenobiSaber());
    }};

    public static void onRenderGlobal(TickEvent.RenderTickEvent e) {
        renderTickTime = e.renderTickTime;
    }

    public static void setLightmapTextureCoords(float x, float y) {
        lastBrightnessX = GLX.lastBrightnessX;
        lastBrightnessY = GLX.lastBrightnessY;
        GLX.glMultiTexCoord2f(GLX.GL_TEXTURE1, x, y);
    }

    public static void restoreLightmapTextureCoords() {
        GLX.glMultiTexCoord2f(GLX.GL_TEXTURE1, lastBrightnessX, lastBrightnessY);
    }

}
