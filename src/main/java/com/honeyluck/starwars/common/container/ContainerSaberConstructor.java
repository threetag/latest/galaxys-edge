package com.honeyluck.starwars.common.container;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.events.Registries;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nullable;

public class ContainerSaberConstructor extends AbstractSaberConstructorContainer {

	public ContainerSaberConstructor(int windowID, World world, BlockPos pos, PlayerInventory playerInventory) {
        super(Registries.saberConstructorContainer, windowID, world, pos, playerInventory);

        getTileEntity().getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(itemHandler -> {
        	Slot handle = new SlotItemHandler(itemHandler, 18, 98, 20);
            addSlot(handle);

			Slot emitter = new SlotItemHandler(itemHandler, 19, 26, 20);
            addSlot(emitter);

            Slot crystal = new SlotItemHandler(itemHandler, 20, 62, 20);
            addSlot(crystal);

            Slot pommel = new SlotItemHandler(itemHandler, 21, 134, 20);
            addSlot(pommel);
        });

		addPlayerInvSlots(playerInventory, 8, 111);
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return isWithinUsableDistance(IWorldPosCallable.of(getTileEntity().getWorld(), getTileEntity().getPos()), playerIn, Registries.saberConstructor);
    }

	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
    	ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = inventorySlots.get(index);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			// Transfer from Lightsaber slot
			if (index >= 18 && index <= 22) {
				// Transfer to Block's inv first, if not then, player's inv
				if (!this.mergeItemStack(itemstack1, 0, 18, false) || !this.mergeItemStack(itemstack1, 19, 55, false)) {
					return ItemStack.EMPTY;
				}
			} else if (index <= 54) { // transfer from block's inv or player's inv
				// if handle slot has an item, endIndex is max, if not, will be next slot after handle
				int endIndex = !inventorySlots.get(18).getStack().isEmpty() ? 23 : 19;

				// Transfer to Lightsaber slots
				if (!this.mergeItemStack(itemstack1, 18, endIndex, false)) {
					return ItemStack.EMPTY;
				}
			}


			if (itemstack1.getCount() == 0) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack1.getCount() == itemstack.getCount()) {
				return ItemStack.EMPTY;
			}

			slot.onTake(playerIn, itemstack1);
		}

		return itemstack;
	}
}
