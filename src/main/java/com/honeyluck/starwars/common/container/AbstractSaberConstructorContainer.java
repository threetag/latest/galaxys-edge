package com.honeyluck.starwars.common.container;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

import javax.annotation.Nullable;

public abstract class AbstractSaberConstructorContainer extends Container {

	private TileEntity tileEntity;

	protected AbstractSaberConstructorContainer(@Nullable ContainerType<?> containerType, int id, World world, BlockPos pos, PlayerInventory playerInventory) {
		super(containerType, id);

		tileEntity = world.getTileEntity(pos);

		tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(itemHandler -> {
			addSlotBox(itemHandler, 0, 8, 60, 9, 18, 2, 18); // inventory
		});
	}

	@Override
	public abstract ItemStack transferStackInSlot(PlayerEntity playerEntity, int slotId);

	int addSlotRange(IItemHandler handler, int index, int x, int y, int amount, int dx) {
		for (int i = 0; i < amount; i++) {
			addSlot(new SlotItemHandler(handler, index, x, y));
			x += dx;
			index++;
		}
		return index;
	}
	int addSlotBox(IItemHandler handler, int index, int x, int y, int horizontal, int dx, int vertical, int dy) {
		for(int j = 0; j < vertical; j++) {
			index = addSlotRange(handler, index, x, y, horizontal, dx);
			y += dy;
		}
		return index;
	}
	void addPlayerInvSlots(PlayerInventory playerInventory, int x, int y) {
		// Player Inventory
		addSlotBox(new InvWrapper(playerInventory), 9, x, y, 9, 18, 3, 18);
		y += 58;
		addSlotRange(new InvWrapper(playerInventory), 0, x, y,9, 18); // Hotbar
	}

	public TileEntity getTileEntity() {
		return tileEntity;
	}
}
