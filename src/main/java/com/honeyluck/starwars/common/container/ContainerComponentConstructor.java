package com.honeyluck.starwars.common.container;

import com.honeyluck.starwars.common.item.ItemBlueprint;
import com.honeyluck.starwars.common.tileentity.TileEntityComponentConstructor;
import com.honeyluck.starwars.events.Registries;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.CraftingResultSlot;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

import javax.annotation.Nullable;

public class ContainerComponentConstructor extends Container {

    private TileEntityComponentConstructor tileEntity;

    public ContainerComponentConstructor(int id, World world, BlockPos pos, PlayerInventory playerInventory) {
        super(Registries.componentConstructorContainer, id);
        tileEntity = (TileEntityComponentConstructor)world.getTileEntity(pos);

        addPlayerInvSlots(playerInventory, 8, 84);
        tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(itemHandler -> {
            addSlotBox(itemHandler, 0, 8, 8, 2, 18, 2, 18);
            addSlot(new ComponentResultSlot(itemHandler, this, 4, 53, 17));
        });
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerEntity, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = inventorySlots.get(index);

        if(slot != null && slot.getHasStack()) {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if(index >= 36 && index <= 40) {
                if (!this.mergeItemStack(itemstack1, 0, 36, false)) {
                    return ItemStack.EMPTY;
                }
            } else {
                if(!this.mergeItemStack(itemstack1, 36, 40, false)) {
                    return ItemStack.EMPTY;
                }
            }

            if (itemstack1.getCount() == 0) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }

            if (itemstack1.getCount() == itemstack.getCount()) {
                return ItemStack.EMPTY;
            }

            slot.onTake(playerEntity, itemstack1);

        }

        return itemstack;
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return isWithinUsableDistance(IWorldPosCallable.of(getTileEntity().getWorld(), getTileEntity().getPos()), playerIn, Registries.componentConstructor);
    }

    public TileEntity getTileEntity() {
        return tileEntity;
    }

    int addSlotRange(IItemHandler handler, int index, int x, int y, int amount, int dx) {
        for (int i = 0; i < amount; i++) {
            addSlot(new SlotItemHandler(handler, index, x, y));
            x += dx;
            index++;
        }
        return index;
    }
    int addSlotBox(IItemHandler handler, int index, int x, int y, int horizontal, int dx, int vertical, int dy) {
        for(int j = 0; j < vertical; j++) {
            index = addSlotRange(handler, index, x, y, horizontal, dx);
            y += dy;
        }
        return index;
    }
    void addPlayerInvSlots(PlayerInventory playerInventory, int x, int y) {
        // Player Inventory
        addSlotBox(new InvWrapper(playerInventory), 9, x, y, 9, 18, 3, 18);
        y += 58;
        addSlotRange(new InvWrapper(playerInventory), 0, x, y,9, 18); // Hotbar
    }

    class ComponentResultSlot extends SlotItemHandler {

        private ContainerComponentConstructor container;

        public ComponentResultSlot(IItemHandler itemHandler, ContainerComponentConstructor container, int index, int xPosition, int yPosition) {
            super(itemHandler, index, xPosition, yPosition);
            this.container = container;
        }

        public ItemStack onTake(PlayerEntity thePlayer, ItemStack stack) {
            this.onCrafting(stack);
            net.minecraftforge.common.ForgeHooks.setCraftingPlayer(thePlayer);
            net.minecraftforge.common.ForgeHooks.setCraftingPlayer(null);
            for(int i = 0; i < 4; ++i) {
                ItemStack itemstack = this.getItemHandler().getStackInSlot(i);
                if (!itemstack.isEmpty() && !(itemstack.getItem() instanceof ItemBlueprint)) {
                    this.getItemHandler().extractItem(i, 1, false);
                    itemstack = this.getItemHandler().getStackInSlot(i);
                }
            }

            return stack;
        }
    }
}
