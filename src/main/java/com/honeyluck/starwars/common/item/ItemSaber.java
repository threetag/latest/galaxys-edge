package com.honeyluck.starwars.common.item;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.client.render.RenderLightsaber;
import com.honeyluck.starwars.events.Registries;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ItemSaber extends Item {
    public static float bladeLength = 1.2F;
    private String model;

    public ItemSaber(String model) {
        super(new Properties().group(StarWars.tabSaberParts).maxStackSize(1).setTEISR(() -> RenderLightsaber::new));
        this.model = model;
        addPropertyOverride(new ResourceLocation("ignited"), (stack, world, entity) -> {
            if (stack.getTag() == null) {
                return 1;
            } else {
                return stack.getTag().getBoolean("ignited") ? 0.0F : 1.0F;
            }
        });
    }

    public String getModel() {
        return model;
    }

    @Nullable
    @Override
    public ICapabilityProvider initCapabilities(ItemStack stack, @Nullable CompoundNBT nbt) {
        return new ICapabilitySerializable<CompoundNBT>() {
            private ItemStackHandler inventory = new ItemStackHandler(3) {
                @Override
                public int getSlotLimit(int slot) {
                    return 1;
                }

                @Override
                public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
                    if(!stack.isEmpty()) {
                        if(stack.getItem() instanceof ItemSaberPart){
                            ItemSaberPart part = (ItemSaberPart) stack.getItem();
                            if(slot == 0 && part.getPartType() == EnumSaberParts.EMITTER || part.getPartType() == EnumSaberParts.CROSSGUARD) {
                                return true;
                            }
                            if(slot == 2 && part.getPartType() == EnumSaberParts.POMMEL) {
                                return true;
                            }
                        }
                        if(slot == 1 && stack.getItem() instanceof ItemKyberCrystal) {
                            return true;
                        }
                    }
                    return false;
                }
            };
            @Override
            public CompoundNBT serializeNBT() {
                CompoundNBT nbt = new CompoundNBT();
                nbt.put("inventory", inventory.serializeNBT());
                return nbt;
            }

            @Override
            public void deserializeNBT(CompoundNBT nbt) {
                this.inventory.deserializeNBT(nbt.getCompound("inventory"));
            }

            @Nonnull
            @Override
            public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
                if(cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
                    return LazyOptional.of(() -> (T) this.inventory);
                }
                return LazyOptional.empty();
            }

        };
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity player, Hand handIn) {
        ItemStack stack = player.getHeldItem(handIn);
        stack.getOrCreateTag();
        stack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(itemHandler -> {
            if(!itemHandler.getStackInSlot(0).isEmpty() && !itemHandler.getStackInSlot(1).isEmpty() && !itemHandler.getStackInSlot(2).isEmpty()) {
                if(player.isSneaking()) {
                    if (!stack.getTag().getBoolean("ignited")) {

                        stack.getTag().putBoolean("ignited", true);
                        if(worldIn.isRemote)
                        worldIn.playSound(player.posX, player.posY, player.posZ, Registries.saber_engage, SoundCategory.PLAYERS, 0.5F, 1, false);

                    } else {

                        stack.getTag().putBoolean("ignited", false);
                        if(worldIn.isRemote)
                        worldIn.playSound(player.posX, player.posY, player.posZ, Registries.saber_disengage, SoundCategory.PLAYERS, 0.5F, 1, false);
                    }
                }
            } else {
                stack.getTag().putBoolean("ignited", false);
            }
        });

        return new ActionResult<>(ActionResultType.PASS, stack);
    }

    @Nullable
    @Override
    public CompoundNBT getShareTag(ItemStack stack) {
        CompoundNBT tag = stack.getOrCreateTag();;
        stack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(itemHandler -> {
            INBT nbt = CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.getStorage().writeNBT(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, itemHandler, null);
            tag.put("inventory", nbt);
        });
        return tag;
    }

    @Override
    public void readShareTag(ItemStack stack, @Nullable CompoundNBT nbt) {
        super.readShareTag(stack, nbt);
        if(nbt != null) {
            stack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(itemHandler -> {
                CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.readNBT(itemHandler, null, nbt.get("inventory"));
            });
        }
    }

    @Override
    public boolean canPlayerBreakBlockWhileHolding(BlockState state, World worldIn, BlockPos pos, PlayerEntity player) {
        if(player.getHeldItemMainhand().getOrCreateTag().getBoolean("ignited")) {
            return !player.isCreative();
        }
        else {
            return true;
        }
    }

    @Override
    public boolean onEntitySwing(ItemStack stack, LivingEntity entity) {
        if(stack.getOrCreateTag().getBoolean("ignited")) {
            if(entity.world.isRemote)
            entity.world.playSound(entity.posX, entity.posY, entity.posZ, Registries.saber_swing, SoundCategory.PLAYERS, 0.5F, 1, false);
        }
        return super.onEntitySwing(stack, entity);
    }

    @Override
    public boolean hitEntity(ItemStack stack, LivingEntity target, LivingEntity attacker) {
        if(stack.getOrCreateTag().getBoolean("ignited")) {
            target.attackEntityFrom(DamageSource.causeMobDamage(attacker), 12);
        }
        return true;
    }

    @Override
    public boolean onDroppedByPlayer(ItemStack item, PlayerEntity player) {
        if(item.getOrCreateTag().getBoolean("ignited") ){
            item.getOrCreateTag().putBoolean("ignited", false);
            if(player.world.isRemote)
            player.world.playSound(player.posX, player.posY, player.posZ, Registries.saber_disengage, SoundCategory.PLAYERS, 0.5F, 1, false);
        }
        return true;
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        stack.getOrCreateTag().putFloat("prevSaberExtension", stack.getOrCreateTag().getFloat("saberExtension"));
        stack.getOrCreateTag().putFloat("prevCrossExtension", stack.getOrCreateTag().getFloat("crossExtension"));

        if (stack.getOrCreateTag().getFloat("saberExtension") < bladeLength && stack.getOrCreateTag().getBoolean("ignited") == true) {
            stack.getOrCreateTag().putFloat("saberExtension", stack.getOrCreateTag().getFloat("saberExtension") + 0.1F);
        }

        if (stack.getOrCreateTag().getFloat("saberExtension") >= bladeLength && stack.getOrCreateTag().getBoolean("ignited") && stack.getOrCreateTag().getFloat("crossExtension") < bladeLength / 4) {
            stack.getOrCreateTag().putFloat("crossExtension", stack.getOrCreateTag().getFloat("crossExtension") + 0.05F);
        }

        if (stack.getOrCreateTag().getFloat("saberExtension") > 0 && stack.getOrCreateTag().getBoolean("ignited") == false) {
            stack.getOrCreateTag().putFloat("saberExtension", stack.getOrCreateTag().getFloat("saberExtension") - 0.1F);
            if (stack.getOrCreateTag().getFloat("crossExtension") > 0) {
                stack.getOrCreateTag().putFloat("crossExtension", stack.getOrCreateTag().getFloat("crossExtension") - 0.05F);
            }
        }

    }

    @Override
    public boolean onEntityItemUpdate(ItemStack stack, ItemEntity entity) {
        stack.getOrCreateTag().putFloat("prevSaberExtension", stack.getOrCreateTag().getFloat("saberExtension"));
            if (stack.getOrCreateTag().getFloat("saberExtension") > 0 && stack.getOrCreateTag().getBoolean("ignited") == false) {
                stack.getOrCreateTag().putFloat("saberExtension", stack.getOrCreateTag().getFloat("saberExtension") - 0.1F);
                if (stack.getOrCreateTag().getFloat("crossExtension") > 0) {
                    stack.getOrCreateTag().putFloat("crossExtension", stack.getOrCreateTag().getFloat("crossExtension") - 0.1F);
                }
            }
        return false;
    }

    @Override
    public ITextComponent getDisplayName(ItemStack stack) {
        AtomicReference<String> name = new AtomicReference<>("");
        stack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(itemHandler -> {
           if(!itemHandler.getStackInSlot(0).isEmpty() && !itemHandler.getStackInSlot(1).isEmpty() && !itemHandler.getStackInSlot(2).isEmpty()) {
               name.set("item."+ StarWars.modid + ".lightsaber.name");
           } else {
               if(!itemHandler.getStackInSlot(0).isEmpty() || !itemHandler.getStackInSlot(1).isEmpty() || !itemHandler.getStackInSlot(2).isEmpty()) {
                   name.set("item." + StarWars.modid + ".incomplete_saber.name");
               } else {
                   name.set("item." + StarWars.modid + ".lightsaber_handle.name");
               }
           }
        });

        return new TranslationTextComponent(name.get());
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        stack.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(itemHandler -> {
            if(!itemHandler.getStackInSlot(0).isEmpty() && !itemHandler.getStackInSlot(1).isEmpty() && !itemHandler.getStackInSlot(2).isEmpty()) {
                tooltip.add(new TranslationTextComponent("display." + StarWars.modid + ".lightsaberInfo"));
            }
        });
    }
}
