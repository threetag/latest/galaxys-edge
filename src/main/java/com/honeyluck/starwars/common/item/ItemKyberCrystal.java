package com.honeyluck.starwars.common.item;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.common.utils.SaberColours;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.awt.*;

public class ItemKyberCrystal extends Item {

    private Vec3d colour = SaberColours.initial;

    public ItemKyberCrystal() {
        super(new Properties().maxStackSize(1).group(StarWars.tabItems));
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity player, Hand handIn) {
        ItemStack stack = player.getHeldItemMainhand();
        if (stack.getTag() == null) {
            stack.setTag(new CompoundNBT());
            this.colour = SaberColours.getRandom(SaberColours.allColours);
            stack.getTag().putInt("red", (int) this.colour.getX());
            stack.getTag().putInt("green", (int) this.colour.getY());
            stack.getTag().putInt("blue", (int) this.colour.getZ());
        }

        return new ActionResult<>(ActionResultType.PASS, player.getHeldItem(handIn));
    }

    @OnlyIn(Dist.CLIENT)
    public static class ItemColor implements IItemColor {
        @Override
        public int getColor(ItemStack stack, int tintIndex) {
            int colour = 0;
            int r, g, b = 0;
            if(stack.hasTag()) {
                r = stack.getTag().getInt("red");
                g = stack.getTag().getInt("green");
                b = stack.getTag().getInt("blue");
            }
            else {
                r = 150;
                g = 250;
                b = 250;
            }
            if (r > 0) colour += r << 16;
            if (g > 0) colour += g << 8;
            if (b > 0) colour += b;
            return colour;
        }
    }
}
