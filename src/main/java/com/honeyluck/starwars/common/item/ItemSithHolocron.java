package com.honeyluck.starwars.common.item;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.client.render.RenderSithHolocron;

public class ItemSithHolocron extends ItemHolocron {

    public ItemSithHolocron() {
        super(false, new Properties().group(StarWars.tabItems).maxStackSize(1).setTEISR(() -> RenderSithHolocron::new));
    }
}
